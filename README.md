# ** Setup do projeto localmente ** #
* Clonar o projeto - git clone https://Zeth12@bitbucket.org/Zeth12/querotrabalharnawpensar.git
* Instalar Anaconda seguindo o seguint link - https://docs.anaconda.com/anaconda/install/linux/
* Criar ambiente virtual - conda create -n venv python=3.6.*
* Ativar ambiente virtual - conda activate venv
* Entrar na pasta app dentro do projeto - cd querotrabalharnawpensar/app
* Instalar dependencias - pip install -r requirements.txt
* Rodar migrações - python manage.py migrate
* Subir servidor local - python manage.py runserver
* Criar usuário admin para acessar admin do django - python manage.py createsuperuser
* Acessar site - http://127.0.0.1:8000/
* Acessar admin - http://127.0.0.1:8000/admin

# ** Acessar projeto no Heroku ** #
* Acessar site - https://tiagoespanhateste.herokuapp.com/
* Acessar admin - Usuário: admin / senha: admin / url: https://tiagoespanhateste.herokuapp.com/admin


# ** Porque trabalhar na WPensar? ** #

* Porque quero trabalhar num ambiente MERITOCRÁTICO. Aqui os melhores são reconhecidos sempre!

* Porque quero aprender coisas novas a cada minuto.

* Porque quero ajudar a construir a maior empresa de tecnologia para educação do Brasil.

* Porque tenho sede por novos desafios.

* Porque quero fazer diferença!

# ** Como faço para me candidatar? ** #

1. Faça um fork do repositório
2. Desenvolva o desafio de acordo com o proposto abaixo
3. Mande um pull request com o curriculo e a resposta do desafio

## ** Caso você não deseje que o envio seja público ** ##

1. Faça um clone do repositório
2. Desenvolva o desafio de acordo com o proposto abaixo
3. Envie um email com o curriculo e um arquivo patch para rh@wpensar.com.br

# **Desafio:** #

O conceito desse desafio é nos ajudar a avaliar as habilidades dos candidatos às vagas de backend.

Você tem que desenvolver um sistema de estoque para um supermercado.

Esse supermercado assume que sempre que ele compra uma nova leva de produtos, ele tem que calcular o preço médio de compra de cada produto para estipular um preço de venda.
Para fins de simplificação assuma que produtos que tenham nomes iguais, são o mesmo produto e que não existe nem retirada e nem venda de produtos no sistema.

O valor calculado de preço médio deve ser armazenado.

Seu sistema deve:

1. Cadastro de produtos (Nome)
2. Compra de produtos (Produto, quantidade e preço de compra)
3. Listagem dos produtos comprados separados por compra (Nome, quantidade, preço de compra, preço médio)
4. Ser fácil de configurar e rodar em ambiente Unix (Linux ou Mac OS X)
5. Ser WEB
6. Ser escrita em Python 3.4+
7. Só deve utilizar biliotecas livres e gratuitas

Esse sistema não precisa ter, mas será um plus:

1. Autenticação e autorização (se for com OAuth, melhor ainda)
2. Ter um design bonito
3. Testes automatizados
4. Usar Jinja Template Engine para fazer o Front End.
5. Disponibilizar o Projeto na Nuvem(Heroku, Aws, ou similar)

# **Avaliação:** #

Vamos avaliar seguindo os seguintes critérios:

1. Você conseguiu concluir os requisitos?
2. Você documentou a maneira de configurar o ambiente e rodar sua aplicação?

# **Condições e Benefícios Oferecidos:** #

* Local: No meio de Icarai, Niterói - RJ
* Regime CLT
* Horário Flexível
* Gestão Horizontal
* Vale Refeição
* Vale Transporte
* Plano de Saúde (CLT)
* Ambiente descontraído
* Grande possibilidade de crescimento
* Trabalhar com projetos que mudam a Educação no País
* Sinuca

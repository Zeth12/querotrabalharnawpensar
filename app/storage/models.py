from django.db import models
from django.contrib.auth.models import User


class Product(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name  # + " Price: " + self.get_price() + "Ammout: " + self.get_ammout()

    def ammout(self):
        ammout = 0
        for purchase in self.purchase_set.all():
            ammout += purchase.ammout

        return ammout

    def price(self):
        price = 0
        for purchase in self.purchase_set.all():
            price += purchase.price

        return (price / self.ammout()) if self.ammout() != 0 else 0


class Purchase(models.Model):
    ammout = models.IntegerField()
    price = models.FloatField()
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    #date = models.DateTimeField()

    def __str__(self):
        return "Product: " + self.product.name + "/Ammout: " + str(self.ammout) + "/Price: " + str(self.price)

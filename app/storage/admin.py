from django.contrib import admin
from .models import Product, Purchase


class PurchaseAdmin(admin.ModelAdmin):
    list_display = ("product", "ammout", "price", "user")

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        super().save_model(request, obj, form, change)

    def get_form(self, request, obj=None, **kwargs):
        self.exclude = ("user",)
        form = super(PurchaseAdmin, self).get_form(request, obj, **kwargs)
        return form


admin.site.register(Purchase, PurchaseAdmin)


class ProductAdmin(admin.ModelAdmin):
    list_display = ("name", "ammout", "price")


admin.site.register(Product, ProductAdmin)

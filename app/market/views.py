from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse
from storage.models import Product


def index(request):
    products = Product.objects.all()
    return render(request, 'market/index.html', {'products': products})


def product_details(request, product_id):
    product = get_object_or_404(Product, id=product_id)
    return render(request, 'market/product_details.html', {'product': product})
